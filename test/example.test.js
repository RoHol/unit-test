const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined

    it('Myvar should exist', () => {
        should.exist(myvar);
    })

    before(() => {

        myvar = 1;
        console.log('Before testing...');

    })

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2); //result expected equal 2
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') //true
    })

    after(() => {
        console.log('After testing..."');
    })
})